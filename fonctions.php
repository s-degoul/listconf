<?php

/* Validation des visioconférences
   - arguments : tableau contenant une visioconférence
   - retourne : booléen */
function valider_parametres_visioconference ($visioconference){
        /* Paramètres des visioconférences qui sont enregistrés dans le fichier FICHIER_VISIOCONFERENCES */
        $parametres_visioconferences_json = array("nom", "URL", "type", "description");
        
        foreach ($parametres_visioconferences_json as $parametre){
                if (! array_key_exists($parametre, $visioconference)){
                        return false;
                }
        }
        return true;
}

/* Récupérer les visioconférences du fichier FICHIER_VISIOCONFERENCES
   - arguments : rien
   - retourne : tableau contenant les visioconférences */
function lire_visioconferences(){
        try {
                $f = fopen(FICHIER_VISIOCONFERENCES, "r");
                if (! $f){
                        throw new Exception("Impossible d'accéder au fichier des visioconférences en lecture");
                }
                $json_visioconferences = fread($f, filesize(FICHIER_VISIOCONFERENCES));
                fclose($f);
                
                $tableau_visioconferences = json_decode($json_visioconferences, true);

                $visioconferences = array();
                foreach($tableau_visioconferences as $id => $visioconference){
                        if (! valider_parametres_visioconference($visioconference)){
                                throw new Exception("Erreurs dans les paramètres de la visioconférence ".$id." contenue dans le fichier");
                        }
                        $visioconferences[] = new Visioconference($id,
                                $visioconference["nom"],
                                $visioconference["URL"],
                                $visioconference["type"],
                                $id + 1,
                                $visioconference["description"]);
                }

                return($visioconferences);
        }
        catch (Exception $e) {
                echo "Erreur dans la lecture des visioconférences : ".$e->getMessage()."\n";
        }
}

/* Écriture des visioconférences dans le fichier FICHIER_VISIOCONFERENCES
   - arguments : tableau contenant les visioconférences
   - retourne : rien */
function ecrire_visioconferences($visioconferences){
        $tableau_visioconferences = array();
        foreach ($visioconferences as $visioconference){
                $tableau_visioconferences[] = array("nom" => $visioconference->getNom(),
                        "URL" => $visioconference->getURL(),
                        "type" => $visioconference->getType(),
                        "description" => $visioconference->getDescription());
        }
        $json_visioconferences = json_encode($tableau_visioconferences, true);

        try {
                $f = fopen(FICHIER_VISIOCONFERENCES, "w");
                if (! $f){
                        throw new Exception("Impossible d'accéder au fichier des visioconférences en écriture");
                }
                fwrite($f, $json_visioconferences);
                fclose($f);
        }
        catch (Exception $e){
                echo "Erreur dans l'enregistrement des visioconférences : ".$e->getMessage()."\n";
        }
}

/* Supprime une visioconférence de la liste
   - arguments : ID de la visioconférence à supprimer, tableau de visioconférence
   - retourne : tableau de visioconférences modifié */
function supprimer_visioconference ($id, $visioconferences, $erreur_si_inexistante = true){
        if (array_key_exists($id, $visioconferences)){
            unset($visioconferences[$id]);
        }
        else if ($erreur_si_inexistante){
                throw new Exception("Impossible de supprimer la visioconférence n° ".$id." (introuvable)");
        }
        return array_values($visioconferences);
}


/* Ajoute une visioconférence à la liste
   - arguments : objet de type Visioconférence, tableau de visioconférences
   - retourne : tableau de visioconférences complété */
function ajouter_visioconference ($visioconference, $visioconferences){
        /* Suppression d'une visioconférence déjà existante */
        /* echo "<pre>";
           print_r($visioconference);
           print_r($visioconferences);
           echo "</pre>"; */
        $visioconferences = supprimer_visioconference($visioconference->getId(), $visioconferences, false);
        /* echo "<pre>";
           print_r($visioconferences);
           echo "</pre>"; */
        $visioconferences[] = $visioconference;
        /* Ajout selon le paramètre "ordre" */
        $ordre = $visioconference->getOrdre();
        if (! $ordre){
                $ordre = 1;
        }
        $nb_visioconf_existantes = count($visioconferences);
        if ($ordre < $nb_visioconf_existantes){
                for ($i = $nb_visioconf_existantes - 1; $i > $ordre - 1; $i--){
                        $visioconferences[$i] = $visioconferences[$i - 1];
                }
                $visioconferences[$ordre - 1] = $visioconference;
        }
        return $visioconferences;
}

