<?php
/***********************************************************************************/
/* Configuration de l'application                                                  */
/* Ne pas modifier une fois en production, à moins de savoir ce que vous faites :) */
/***********************************************************************************/

  /* Debuggage */
  /* TODO : À COMMENTER EN PRODUCTION !! */
error_reporting(E_ALL);
ini_set("display_errors",1);
ini_set("error_reporting", E_ALL);
ini_set("display_startup_errors",1);
error_reporting(-1);

/* Version de l'application */
define("VERSION", "1.0");

/* Persistance des données */
/* - paramètres */
define("FICHIER_PARAMETRES", "parametres.json");
/* - liste des visioconférences */
define("FICHIER_VISIOCONFERENCES", "visioconferences.json");

/* Fichier de licence */
define("FICHIER_LICENSE", "LICENSE");

/* URL vers le dépot Git contenant les sources */
define("URL_SOURCES", "https://framagit.org/favdb/listconf");

/* URL vers la documentation */
define("URL_DOCUMENTATION", "https://framagit.org/favdb/listconf/-/wikis/home");

/* URL vers la MJC */
define("URL_MJC", "https://mjc-chevalblanc.fr/");

/* Types d'utilisateur */
$TYPES_UTILISATEURS = array(1 => array("nom" => "super administrateur",
                                       "description" => "A tous les droits sur le paramétrage"),
                            2 => array("nom" => "administrateur",
                                       "description" => "Droits limités aux visioconférences non officielles"));

/* Type de visioconférence */
$TYPES_VISIOCONFERENCES = array(1 => array("nom" => "officielle",
                                           "icone" => "fas fa-user-graduate",
                                           "droits" => array("creation" => array(1),
                                                             "modification" => array(1))),
                                2 => array("nom" => "thématique",
                                           "icone" => "fas fa-star",
                                           "droits" => array("creation" => array(),
                                                             "modification" => array(1,2))),
                                3 => array("nom" => "temporaire",
                                           "icone" => "fas fa-comment",
                                           "droits" => array("creation" => array(),
                                                             "modification" => array(1,2))));

/* Question de securite et réponse secrète */
define("QUESTION_SECURITE", "Dans quelle ville est domiciliée la MJC ? (en minuscules)");
define("REPONSE_SECRETE", "wintzenheim");
