<p class="menu-titre">
    <a href="index.php"><span class="fas fa-home"></span> Accueil</a>
</p>
<?php
/* Affichage des autres items du menu seulement s'il s'agit de la page d'accueil (pas de paramètre "page" passé dans la requête HTTP) */
if (! isset ($_REQUEST["page"])){
?>
    <div class="liste-visioconferences">
        <p class="menu-titre">
            Liste des visioconférences
        </p>
        <p>
            <a href="?page=edition_visioconference&action=ajouter">
                <span class="fas fa-plus-circle"></span> Ajouter
            </a>
        </p>
        <?php
        if (count($visioconferences) > 0){
                echo "<p class=\"menu-info\">Survoler les liens avec la souris pour plus d'informations</p>";
                echo "\n<ol>\n";
                foreach ($visioconferences as $i => $visioconference){
                        echo "<li>\n<a href=\"".$visioconference->getURL()."\" target=\"salon\" title=\""."[".$TYPES_VISIOCONFERENCES[$visioconference->getType()]["nom"]."] ".$visioconference->getDescription()."\">"
                                ."<span class=\"".$TYPES_VISIOCONFERENCES[$visioconference->getType()]["icone"]."\"></span> "
                                .$visioconference->getNom()
                                ."</a>\n"
                                ."<span class=\"edition-visioconference\">\n"
                                ."<a href=\"?page=edition_visioconference&id=".$visioconference->getId()."&action=editer\"><span class=\"fas fa-edit\"></span></a>\n"
                                ."<a href=\"?page=edition_visioconference&id=".$visioconference->getId()."&action=supprimer\"><span class=\"fas fa-trash\" onclick=\"return confirm('Êtes-vous sûr(e) de supprimer cette visioconférence ? (irréversible)')\"></span></a>\n"
                                ."</span>\n"
                        /* ."<span class=\"description-visioconference\">"
                           .$visioconference->getDescription()
                           ."</span>\n" */
                                ."</li>\n";
                }
                echo "</ol>\n";
        }
        else {
                echo "<p>Aucune visioconférence</p>\n";
        }
        ?>
    </div>
<?php } ?>
<p class="menu-titre">
    <a href="<?php echo URL_DOCUMENTATION; ?>" target="blank"><span class="fas fa-book"></span> Documentation <span class="fas fa-external-link-alt" style="font-size: 0.6em"></span></a>
</p>
<p>
    <a href="<?php echo URL_MJC; ?>" target="blank">Site de la MJC du Cheval Blanc <span class="fas fa-external-link-alt" style="font-size: 0.6em"></span></a>
</p>
