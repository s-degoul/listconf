<?php

class Visioconference {
    /* Identifiant le temps de la session = n° d'entrée dans le fichier, valeur non enregistrée dans le fichier */
    private $id;
    /* Nom de la visioconférence. Obligatoire. Doit être unique */
    private $nom;
    /* URL vers la visioconférence. Obligatoire. Doit être unique */
    private $URL;
    /* Type de visioconférence. Obligatoire */
    private $type;
    /* Ordre d'apparition */
    private $ordre;
    /* Description */
    private $description;

    public function __construct($id = null, $nom = "", $URL = "", $type = null, $ordre = null, $description = ""){
        $this->setId($id);
        $this->setNom($nom);
        $this->setURL($URL);
        $this->setType($type);
        $this->setOrdre($ordre);
        $this->setDescription($description);
    }

    public function getId(){
        return $this->id;
    }
    /* Accessible uniquement pas le constructeur */
    private function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getNom(){
        return $this->nom;
    }
    public function setNom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function getURL(){
        return $this->URL;
    }
    public function setURL($URL){
        $this->URL = $URL;
        return $this;
    }

    public function getType(){
        return $this->type;
    }
    public function setType($type){
        $this->type = $type;
        return $this;
    }
    
    public function getOrdre(){
        return $this->ordre;
    }
    public function setOrdre($ordre){
        $this->ordre = intval($ordre);
        return $this;
    }

    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    public function valider($visioconferences){
        $erreurs = array();
        if (! $this->nom){
            $erreurs[] = "Une visioconférence doit avoir un nom";
        }
        if (! $this->URL or ! filter_var($this->URL, FILTER_VALIDATE_URL)){
            $erreurs[] = "Une visioconférence doit avoir une URL valide";
        }
        if (! $this->type){
            $erreurs[] = "Une visioconférence doit avoir un type valide";
        }

        /* Unicité des valeurs */
        foreach($visioconferences as $visioconference){
            if ($this->id != $visioconference->getId()){
                if ($this->nom == $visioconference->getNom()){
                    $erreurs[] = "Une autre visioconférence avec le même nom existe déjà";
                }
                if ($this->URL == $visioconference->getURL()){
                    $erreurs[] = "Une autre visioconférence avec la même URL existe déjà";
                }
            }
        }

        return $erreurs;
    }
  }