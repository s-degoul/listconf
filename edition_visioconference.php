<?php
/* Détermination de l'action à effectuer selon les paramètres GET ou POST */
if (isset($_POST["id"])){
        $action = "enregistrer";

        // TODO : protection contre l'injection de code (htmlspecialchars..)
        
        /* Validation des données soumises par le formulaire */
        $ordre = intval(htmlspecialchars($_POST["ordre"]));
        if ($ordre == 0){
                $ordre = count($visioconferences);
        }
        $visioconference = new Visioconference(
                htmlspecialchars($_POST["id"]),
                        htmlspecialchars($_POST["nom"]),
                        htmlspecialchars($_POST["URL"]),
                        htmlspecialchars($_POST["type"]),
                        $ordre,
                        htmlspecialchars($_POST["description"]));
        $erreurs = $visioconference->valider($visioconferences);
        if ($_POST["reponse_secrete"] != REPONSE_SECRETE){
                $erreurs[] = "Mauvaise réponse à la question secrète";
        }
        /* Si erreur, abandon de l'enregistrement et resoumission du formulaire avec les données saisies */
        if ($erreurs){
                echo "<p>\nErreur dans les données soumises\n<ul>\n";
                foreach($erreurs as $erreur){
                        echo "<li>".$erreur."</li>\n";
                }
                echo "</ul>\n</p>\n";
                $action = "editer";
        }
}
else if (isset($_GET["action"])){
        $action = $_GET["action"];
}
else {
        $action = "ajouter";
}

/* Gestion d'un enregistrement de données : modification (envoi de formulaire) ou suppression */
if ($action == "enregistrer" or $action == "supprimer"){
        try {
                switch($action){
                        case "enregistrer":
                                $visioconferences = ajouter_visioconference($visioconference, $visioconferences);
                                break;
                        case "supprimer":
                                if (! isset($_GET["id"])){
                                        throw new Exception("Aucun ID fourni");
                                }
                                $visioconferences = supprimer_visioconference($_GET["id"], $visioconferences);
                                break;
                }

                ecrire_visioconferences($visioconferences);
                header("location:index.php");
        }
        catch (Exception $e){
                echo "Erreur dans la modification de la visioconférence : ".$e->getMessage();
        }
}
else {
        /* Gestion d'un ajout ou d'une demande de modification : affichage du formulaire */
        try{
                switch($action){
                        case "ajouter":
                                $visioconference = new Visioconference();
                                break;
                        case "editer":
                                /* Cas de la soumission du formulaire déjà traité plus haut (contrôle des données) */
                                /* Donc on traite de cas du "primo" chargement du formulaire de modification */
                                if (! isset($_POST["id"])){
                                        if (! isset($_GET["id"])){
                                                throw new Exception("Aucun ID de visioconference fourni");
                                        }
                                        $id = $_GET["id"];
                                        if (! array_key_exists($id, $visioconferences)){
                                                throw new Exception("ID de visioconférence fourni invalide");
                                        }
                                        $visioconference = $visioconferences[$id];
                                }
                                break;
                        default:
                                throw new Exception("Action ".$action." inconnue");
                }
        }
        catch (Exception $e){
                echo "Erreur : ".$e->getMessage();
        }
}

?>

<div id="formulaire_visioconference">
    <form action="index.php" method="post">
        <input type="hidden" name="page" value="edition_visioconference" />
        <input type="hidden" name="id" value="<?php echo $visioconference->getId(); ?>" />
        <div class="form-group">
            <label for="nom">Nom *</label>
            <input type="text" name="nom" value="<?php echo $visioconference->getNom(); ?>" class="form-control" required placeholder="Choisir un nom parlant"/>
        </div>
        <div class="form-group">
            <label for="URL">URL *</label>
            <input type="text" name="URL" value="<?php echo $visioconference->getURL(); ?>" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="type">Type *</label>
            <select name="type" class="form-control">
                <?php
                /* TODO : afficher les options selon les droits utilisateurs ($caracteristiques["droits"]) */
                foreach ($TYPES_VISIOCONFERENCES as $id => $caracteristiques){
                        $selected = "";
                        if (! is_null($visioconference->getType()) and $id == $visioconference->getType()){
                                $selected = "selected";
                        }
                        echo "<option value=\"".$id."\"".$selected.">".$caracteristiques["nom"]."</option>\n";                        
                }
                ?>
            </select>
        </div>

        <!-- TODO : readonly pour utilisateur non authentifié -->
        <div class="form-group">
            <label for="ordre">Ordre d'apparition dans le menu</label>
            <select name="ordre" class="form-control">
                <option value="0"></option>
                <?php
                for ($i = 1; $i <= count($visioconferences) + 1; $i ++){
                        $selected = "";
                        if ($i == $visioconference->getOrdre()){
                                $selected = "selected";
                        }
                        echo "<option value=\"".$i."\"".$selected.">".$i."</option>\n";
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" name="description" class="form-control" placeholder="À quoi sert la visioconférence ?"><?php echo $visioconference->getDescription(); ?></textarea>
        </div>
        <div class="form-group">
            <label for="reponse_secrete">Sécurité : <?php echo QUESTION_SECURITE; ?> *</label>
            <input type="text" name="reponse_secrete" value="" class="form-control" required />
        </div>

        <button type="submit" class="btn btn-primary">Enregistrer</button>
        <a href="index.php">Annuler</a>
    </form>
</div>

