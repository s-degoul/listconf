<?php
include_once("config_interne.php");
include_once("classes/visioconference.php");
include_once("fonctions.php");

$visioconferences = lire_visioconferences();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>MJC Cheval Blanc - Gestion des visioconférences</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html" />
        <link rel="stylesheet" href="assets/fontawesome-free-5.15.2-web/css/all.min.css" type="text/css" />
        <link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="assets/style.css" type="text/css" />
    </head>
    <body>
        <div class="container page">
            <header class="page-header">
                <h1>MJC Cheval Blanc - Gestionnaire de visioconférences</h1>
            </header>
            <div id="corps" class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <?php
                        include("menu.php");
                        ?>
                    </div>

                    <div class="col-lg-9">
                        <?php
                        if (isset ($_REQUEST["page"])){
                                $page = $_REQUEST["page"];
                                include($page.".php");
                        }
                        else {
                                echo "<iframe name=\"salon\" src=\"\" title=\"salon\" style=\"width: 100%; height: 100%\"></iframe>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <footer>
                <div class="row">
                    <div class="col-lg-6">
                        <p>
                            Programme sous licence libre
                            <a href="<?php echo FICHIER_LICENSE; ?>"><span class="fab fa-linux"></span>&nbsp;Consulter&nbsp;la&nbsp;licence</a>&nbsp;&nbsp;
                            <a href="<?php echo URL_SOURCES; ?>" target="blank"><span class="fab fa-gitlab"></span>&nbsp;Code&nbsp;source&nbsp;<span class="fas fa-external-link-alt" style="font-size: 0.6em"></span></a>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <p>
	                    Version <?php echo VERSION; ?>
                        </p>
                    </div>
            </footer>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    </body>
</html>
