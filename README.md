
ListConf

Système de gestion de salons virtuels

J'écris autre chose

[[_TOC_]]

# Structure du programme

## Scripts

Les fichiers mélangent du code PHP et de l'HTML.

Organisation :

- `index.php` : page principale sur laquelle on "reste" toujours ; y sont inclus le menu, et le contenu principal de la page via le paramètre `page` de la requête HTTP, en GET (dans l'URL), parfois en POST (en cas de soumission de formulaire),
- `config_interne.php` : définition des constantes permettant de configurer le fonctionnement du programme,
- `fonctions.php` : fonctions utilisées dans les autres fichiers,
- `classes/visioconference.php` : déclaration de la classe `Visioconference`, permettant de manipuler les visioconférences sous forme d'objet,
- `menu.php` : menu permettant d'afficher la liste des conférences et d'accéder aux fonctions d'administration (ajout, modification et suppression de conférences),
- `edition_visioconference.php` : formulaire d'édition des visioconférences, qu'il s'agisse d'ajout, de modification ou de suppression,
- `edition_parametres.php` : pour éditer les paramètres. Non utilisé pour l'instant.

## Persistance des données

Choix du format [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation) :

- permet un contenu structuré, ce qui est plus difficile à faire avec du TXT,
- facilement *parsable* par PHP : fonctions [json_decode](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation) et [json_encode](https://www.php.net/manual/fr/function.json-encode.php),
- assez simple pour permettre une édition manuelle, contrairement aux XML par exemple.

Fichiers :

- `visioconferences.json` : liste des visioconférences actives, avec leurs caractéristiques. Contenu du fichier écrasé à chaque appel du script `edition_visioconference.php`
- `parametres.json` : paramètres, tels les comptes administrateurs, les serveurs de visioconférences. Non utilisé pour l'instant.

## Design, interactivité

Dossier `assets` : dépendances de code CSS et Javascript pour rendre le tout plus convivial.

- `style.css` : feuille de style personnalisée
- utilisation de *frameworks* :
  - [Bootstrap 3.3](https://getbootstrap.com/docs/3.3/) pour le [/design responsive/](https://fr.wikipedia.org/wiki/Site_web_r%C3%A9actif)
  - [Fontawesome 5.15](https://fontawesome.com/) pour les icônes.

# Installation

Cloner le projet dans un répertoire accessible par le serveur web installé sur votre machine (Apache2, Nginx...). Par exemple, avec Apache2, **et uniquement pour test**, répertoire */home/votre_user/public_html/* si le [*mod_userdir*](https://httpd.apache.org/docs/2.4/fr/mod/mod_userdir.html) est activé.

```bash
git clone https://framagit.org/favdb/listconf
```

Donner les droits d'accès suffisant au serveur web, en attribuant au minimum le groupe lié au serveur web à tous les fichiers : *www-data* (pour Apache2 sur les systèmes Debian-like), *httpd*...

```bash
# Groupe
sudo chgrp -R www-data listconf/
# voir même : sudo chown -R www-data:www-data listconf/ mais problème = l'utilisateur n'a plus accès aux fichiers en écriture (à moins de faire un chmod -R 777... berk)

# Droits d'accès aux fichiers
# en lecture pour fichiers et répertoire
find listconf -type f -exec chmod 640 {} \;
find listconf -type d -exec chmod 750 {} \;
# mais l'écriture par le serveur web doit être possible pour les fichiers json
chmod 660 listconf/visioconferences.json
chmod 660 listconf/parametres.json
```

Accès via un navigateur. Dans le cas d'une installation avec le *mod_userdir*, [](http://localhost/~votre_user/listconf/index.php)


# Licence

Voir le fichier `LICENSE`

# À faire

Il ne s'agit pour l'instant que d'une ébauche.

Liste de fonctionnalités à ajouter / corrections :

- IMPORTANT : les salons Jitsi ne semblent pas bien fonctionner dans une *iframe* (problème pour demander au navigateur le droit d'accéder au micro et à la webcam). À étudier ++.
  - À défaut, ouvrir les visioconférences dans un autre onglet, **en prévenant les utilisateurs** +/- détecter la présence d'une visioconférence en cours dans un autre onglet pour "désactiver" les liens vers les autres.
- utiliser l'API de Jistsi  https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe permettant notamment de recueillir le nombre instantané de participants (rafraichissement toutes les x secondes ?)
- gestion de paramètres, persistés dans le fichier `parametres.json` :
  - administrateurs : identifiant + mot de passe
  - URL des plateformes de visioconférences conseillées -> aider au renseignement du champ "URL" du formulaire de création/modification d'une visioconférence (tout en laissant le choix de saisir une URL personnalisée)
- sécurité :
  - se connecter pour accéder aux fonctions d'administration = droits d'édition des paramètres et des visioconférences (affichage des boutons "modifier" et "supprimer")
  - un utilisateur non connecté peut créer une conférence. Prévention anti-robot : réponse à une question secrète (qui n'apparaît pas s'il l'on est connecté comme administrateur)
- améliorer le design, notamment la taille de l'*iframe* (si on la maintient)

